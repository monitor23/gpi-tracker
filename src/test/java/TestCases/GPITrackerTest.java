package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.GPIHome;
import ObjectRepositories.GPILogin;

public class GPITrackerTest {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://netlink04.netlink-testlabs.com/swp/customgroup/gpi/");

	}

	@Test
	public void GPILogin() throws InterruptedException {
		GPILogin gl = new GPILogin(driver);
		gl.WaitFunction();
		gl.Username().sendKeys("netlink05");
		gl.Password().sendKeys("July2021July2021+");
		gl.Login().click();
		GPIHome gh = new GPIHome(driver);
		gh.GetTitle();

	}

}
